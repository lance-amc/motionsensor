/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    main.c
 * @author  lance
 *
 ********************************************************************************************************
 */


#include "stm8s.h"
#include "stm8s_gpio.h"
#include "Eeprom_ext.h"
#include "stm8s_flash.h"
#include "OTA.h"

#define SWCR_SWEN_BIT   1
#define SWCR_SWBSY_BIT  0
/*
 * set system clock to 16M
*/
void init_sysClock(void)
{
    CLK->ECKR |= CLK_ECKR_HSEEN;
    while(!(CLK->ECKR & 0x02)) ;
    
    CLK->SWCR |=  (1 << SWCR_SWEN_BIT);   // set bit SWEN to 1
    CLK->SWR = 0xB4;          // write SWR to select target clock source
    while(CLK->SWCR & (1 << SWCR_SWBSY_BIT)) ;           // wait for SWBSY cleared 
}


int main( void )
{
    sim();               // disable interrupts 
    init_sysClock();
    EEPROM_Init();
    //
    
    volatile uint16_t i = 0xFFFF;
    
    while(i--);
    
    if(OTA_Check())
    {
        GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST);
        FLASH_Unlock(FLASH_MEMTYPE_PROG);
        while (FLASH_GetFlagStatus(FLASH_FLAG_PUL) == RESET);
        OTA_Update();
        FLASH_Lock(FLASH_MEMTYPE_PROG);
        OTA_ClearUpdateFlag();
    }
        
    asm("LDW X,  SP ");
    asm("LD  A,  $FF");
    asm("LD  XL, A  ");
    asm("LDW SP, X  ");
    asm("JPF $9000");

    while(1) {
    ;
    }

    return 0;
}
