/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    comm_data.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#ifndef COMM_DATA_H
#define COMM_DATA_H

#include "stm8s.h"

typedef void (*CommData_SetCB)(uint8_t data);

void CommData_SetMotionSensorOn(void);
void CommData_SetMotionSensorOff(void);
uint8_t CommData_GetMotionSensorState(void);

uint16_t CommData_GetProductLifeTime(void);
void CommData_SetProductLifeTime(uint16_t newLifeTime);
uint8_t CommData_GetTriggerState(void);
void CommData_UpdateMsState(uint8_t state);

uint8_t CommData_GetSensitivity(void);
void CommData_SetSensitivity(uint8_t data);

uint8_t CommData_GetRangeOfDetection(void);
void CommData_SetRangeOfDetection(uint8_t range);

uint16_t CommData_GetLightLevel(void);
void CommData_SetLightLevel(uint16_t newLightLevel);
uint16_t CommData_GetLightSensor(uint8_t addr);


#endif //COMM_DATA_H