/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    MemPool.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#ifndef __MEM_POOL_H
#define __MEM_POOL_H

#include "stm8s.h"

#ifdef __cplusplus
extern "C" {
#endif

void MemPool_Init(void);
void* MemPool_Get(uint8_t size);
void MemPool_Put(void *p);


#ifdef __cplusplus
}
#endif

#endif
