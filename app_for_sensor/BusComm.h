#ifndef BUS_COMM_H
#define BUS_COMM_H

#ifdef __cplusplus
extern "C" {
#endif
	
#include <stdbool.h>
	
void BusComm_Init(void);	
bool BusComm_IsIdle(void);
	
#ifdef __cplusplus
}
#endif

#endif