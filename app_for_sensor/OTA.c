/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    OTA.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "OTA.h"
#include "stm8s_flash.h"
#include "system.h"

#define APP_CODE_START_ADDR             ((uint16_t)36864)
#define APP_BLOCK_NUM_START             ((uint16_t)(APP_CODE_START_ADDR - 0x8000)/128)

#define OTA_CODE_START_ADDR				((uint16_t)(APP_CODE_START_ADDR + 14 * 1024))
#define OTA_MAX_PACKAGE_CODE_SIZE		(128)
#define OTA_BLOCK_NUM_START             ((uint16_t)(OTA_CODE_START_ADDR - 0x8000)/128)

//mask for memory block boundary
#define  BLOCK_BYTES          128
#define  ADDRESS_BLOCK_MASK   127 	/*(BLOCK_BYTES - 1)*/

//memory boundaries
#define  RAM_START            0x000000ul
#define  RAM_END              0x0007FFul
#define  OPTION_START         0x004800ul
#define  OPTION_END           0x0048FFul
#define  UBC_OPTION_LOCATION  0x004801ul
#define  BLOCK_SIZE           0x80
#define  BLOCK_PER_SECTOR     0x08

#define  FLASH_START          0x008000ul
#define  FLASH_END            0x02FFFFul
#define  FLASH_BLOCKS_NUMBER  0x500 /*((FLASH_END  - FLASH_START  + 1)/BLOCK_SIZE)*/
#define  SECTORS_IN_FLASH     0xA0 /*((FLASH_END  - FLASH_START  + 1)/BLOCK_SIZE/BLOCK_PER_SECTOR)*/ 
#define  FLASH_CLEAR_BYTE     0xA5

#define  EEPROM_START         0x004000ul
#define  EEPROM_END           0x0043FFul
#define  EEPROM_BLOCKS_NUMBER 0x08 /*((EEPROM_END - EEPROM_START + 1)/BLOCK_SIZE)*/
#define  SECTORS_IN_EEPROM    0x01 /*((EEPROM_END - EEPROM_START + 1)/BLOCK_SIZE/BLOCK_PER_SECTOR)*/

#define  SECTORS_COUNT        0x21 /*(SECTORS_IN_FLASH + SECTORS_IN_EEPROM)*/
#define  MAX_SECTOR_NUMBER    0x20 /*(SECTORS_IN_FLASH + SECTORS_IN_EEPROM - 1)*/

//static uint16_t total_packages;
//static uint32_t fw_checksum;
static uint16_t package_number = 0;

__no_init __eeprom uint16_t OTA_Flag @0x4000;
__no_init __eeprom uint16_t OTA_Bin_Size @0x4002;

//void Mem_ProgramBlock(uint16_t BlockNum, FLASH_MemType_TypeDef MemType, uint8_t *Buffer);
//void FLASH_ProgramOptionByte(uint16_t Address, uint8_t Data);
//
//uint8_t CheckAddress(uint32_t DataAddress){
//  //for Flash
//  if ((DataAddress >= FLASH_START) && (DataAddress <= FLASH_END))
//    return 1;
//  else
//    //for EEPROM
//    if ((DataAddress >= EEPROM_START) && (DataAddress <= EEPROM_END))
//      return 1;
//    else
//      //for RAM
//      if ((DataAddress >= RAM_START) && (DataAddress <= RAM_END))
//        return 1;
//      else
//        //for Option bytes
//        if ((DataAddress >= OPTION_START) && (DataAddress <= OPTION_END))
//          return 1;
//  return 0;
//}//CheckAddress
//
//uint8_t WriteBufferFlash(uint8_t FAR* DataAddress, uint8_t DataCount, FLASH_MemType_TypeDef MemType);
//
//uint8_t WriteBuffer(uint8_t FAR* DataAddress, uint8_t DataCount){
//  uint8_t i;
//  
//  //for Flash
//  if (((uint32_t)DataAddress >= FLASH_START) && (((uint32_t)DataAddress + DataCount - 1) <= FLASH_END))
//    return WriteBufferFlash(DataAddress, DataCount, FLASH_MEMTYPE_PROG);
//    
//  //for EEPROM
//  if (((uint32_t)DataAddress >= EEPROM_START) && (((uint32_t)DataAddress + DataCount - 1) <= EEPROM_END))
//    return WriteBufferFlash(DataAddress, DataCount, FLASH_MEMTYPE_DATA);
//    
//  //for RAM
//  if (((uint32_t)DataAddress >= RAM_START) && (((uint32_t)DataAddress + DataCount - 1) <= RAM_END))
//  {
////    for(i=0; i<DataCount; i++)
////      DataAddress[i] = DataBuffer[i];
//    return 1;
//  }
//  
//  //for Option bytes
//  if (((uint32_t)DataAddress >= OPTION_START) && (((uint32_t)DataAddress + DataCount - 1) <= OPTION_END))
//  {
//    for(i=0; i<DataCount; i++)
//    {
//       FLASH_ProgramOptionByte((uint32_t)(&DataAddress[i]), DataBuffer[i]);
//    }
//    return 1;
//  }
//  
//  //otherwise fail
//  return 0;
//}//WriteBuffer
//
//uint8_t WriteBufferFlash(uint8_t FAR* DataAddress, uint8_t DataCount, FLASH_MemType_TypeDef MemType){
//  uint16_t Address = (uint16_t) DataAddress;
//  uint8_t * DataPointer = DataBuffer;
//  uint32_t Offset;
//  //set offset according memory type
//  if(MemType == FLASH_MEMTYPE_PROG)
//    Offset = FLASH_START;
//  else
//    Offset = EEPROM_START;
//  //program beginning bytes before words
//  while((Address % 4) && (DataCount))
//  {
//    *((PointerAttr uint8_t*) Address) = *DataPointer;
//		while( (FLASH->IAPSR & (FLASH_IAPSR_EOP | FLASH_IAPSR_WR_PG_DIS)) == 0);
//		Address++;
//    DataPointer++;
//    DataCount--;
//	}
//  //program beginning words before blocks
//	while((Address % BLOCK_BYTES) && (DataCount >= 4))
//  {
//		FLASH->CR2 |= (uint8_t)0x40;
//		FLASH->NCR2 &= (uint8_t)~0x40;
//		*((PointerAttr uint8_t*)Address) = (uint8_t)*DataPointer  ; 	 /* Write one byte - from lowest address*/
//		*((PointerAttr uint8_t*)(Address + 1)) = *(DataPointer + 1); /* Write one byte*/
//		*((PointerAttr uint8_t*)(Address + 2)) = *(DataPointer + 2); /* Write one byte*/
//		*((PointerAttr uint8_t*)(Address + 3)) = *(DataPointer + 3); /* Write one byte - from higher address*/
//    while( (FLASH->IAPSR & (FLASH_IAPSR_EOP | FLASH_IAPSR_WR_PG_DIS)) == 0);
//		Address    += 4;
//    DataPointer+= 4;
//    DataCount  -= 4;
//  }
//  //program blocks
//  while(DataCount >= BLOCK_BYTES)
//  {
//    Mem_ProgramBlock((Address - Offset)/BLOCK_BYTES, MemType, DataPointer);
//    Address    += BLOCK_BYTES;
//    DataPointer+= BLOCK_BYTES;    
//    DataCount  -= BLOCK_BYTES;
//  }
//  
//  //program remaining words (after blocks)
//  while(DataCount >= 4)
//  {
//		FLASH->CR2 |= (uint8_t)0x40;
//		FLASH->NCR2 &= (uint8_t)~0x40;
//		*((PointerAttr uint8_t*)Address) = (uint8_t)*DataPointer  ; 	 /* Write one byte - from lowest address*/
//		*((PointerAttr uint8_t*)(Address + 1)) = *(DataPointer + 1); /* Write one byte*/
//		*((PointerAttr uint8_t*)(Address + 2)) = *(DataPointer + 2); /* Write one byte*/
//		*((PointerAttr uint8_t*)(Address + 3)) = *(DataPointer + 3); /* Write one byte - from higher address*/
//    while( (FLASH->IAPSR & (FLASH_IAPSR_EOP | FLASH_IAPSR_WR_PG_DIS)) == 0);
//		Address    += 4;
//    DataPointer+= 4;
//    DataCount  -= 4;
//  }
//  
//  //program remaining bytes (after words)
//  while(DataCount)
//  {
//    *((PointerAttr uint8_t*) Address) = *DataPointer;
//    while( (FLASH->IAPSR & (FLASH_IAPSR_EOP | FLASH_IAPSR_WR_PG_DIS)) == 0);
//    
//	Address++;
//    DataPointer++;
//    DataCount--;
//  }
//  
//  return 1;
//}//WriteBufferFlash
//	
//
//IN_RAM(void Mem_ProgramBlock(uint16_t BlockNum, FLASH_MemType_TypeDef MemType, uint8_t *Buffer))
//{
//    uint16_t Count = 0;
//    uint32_t StartAddress = 0;
//    uint16_t timeout = (uint16_t)0x6000;
// 
// /* Set Start address wich refers to mem_type */
//    if (MemType == FLASH_MEMTYPE_PROG)
//    {
//        StartAddress = FLASH_START;
//    }
//    else
//    {
//        StartAddress = EEPROM_START;
//    }
//
//    /* Point to the first block address */
//    StartAddress = StartAddress + ((uint32_t)BlockNum * BLOCK_SIZE);
//
//    /* Standard programming mode */ 
//    FLASH->CR2 |= (uint8_t)0x01;
//    FLASH->NCR2 &= (uint8_t)~0x01;
//    
//    /* Copy data bytes from RAM to FLASH memory */
//    for (Count = 0; Count < BLOCK_SIZE; Count++)
//    {
//        *((PointerAttr uint8_t*)StartAddress + Count) = ((uint8_t)(Buffer[Count]));
//    }
//
// #if defined (STM8S208) || defined(STM8S207) || defined(STM8S105)
//    if (MemType == FLASH_MEMTYPE_DATA)
//    {
//        /* Waiting until High voltage flag is cleared*/
//        while ((FLASH->IAPSR & 0x40) != 0x00 || (timeout == 0x00))
//        {
//            timeout--;
//        }
//    }
// #endif /* STM8S208, STM8S207, STM8S105 */
//}
//
////#ifdef _COSMIC_
////#pragma section ()
////#endif /* __COSMIC__ */
////
////void FLASH_ProgramOptionByte(uint16_t Address, uint8_t Data){
////	uint8_t flash_iapsr;	
////  /* Enable write access to option bytes */
////  FLASH->CR2 |= (uint8_t)0x80;
////  FLASH->NCR2 &= (uint8_t)(~0x80);
////
////  /* Program option byte and his complement */
////  *((NEAR uint8_t*)Address) = Data;
////  *((NEAR uint8_t*)(Address + 1)) = (uint8_t)(~Data);
////
////	#if defined (STM8S208) || defined(STM8S207) || defined(STM8S105)
////	  flash_iapsr = FLASH->IAPSR ;
////		while (!(flash_iapsr & 0x41)) flash_iapsr = FLASH->IAPSR ;
////	#else /*STM8S103, STM8S903*/
////	  flash_iapsr = FLASH->IAPSR ;
////		while (!(flash_iapsr & 0x05)) flash_iapsr = FLASH->IAPSR ;
////	#endif /* STM8S208, STM8S207, STM8S105 */
////
////  /* Disable write access to option bytes */
////  FLASH->CR2 &= (uint8_t)(~0x80);
////  FLASH->NCR2 |= (uint8_t)0x80;
////}

static uint16_t totalLength;
static uint8_t* currentPos;

void OTA_Prepare(void)
{
    ;
}

void OTA_Start(uint16_t binLength)
{
	package_number = 0;
    currentPos = (uint8_t *)OTA_CODE_START_ADDR;
    totalLength = binLength;
    OTA_Bin_Size = binLength;
}

bool OTA_Write(uint8_t *data, uint8_t size)
{
    if(++package_number != (*data + ((*(data+1)) << 8)))
        return false;
    FLASH_Unlock(FLASH_MEMTYPE_PROG);
    FLASH_ProgramBlock(OTA_BLOCK_NUM_START + package_number - 1, FLASH_MEMTYPE_PROG, FLASH_PROGRAMMODE_STANDARD, data+2);
    currentPos += 128;
    
    if(totalLength <= package_number * 128) {
        OTA_SetUpdateFlag();
        FLASH_Lock(FLASH_MEMTYPE_PROG);
        set_restart_flag();
    }
    
    return true;
           
}

void OTA_SetUpdateFlag(void)
{
    OTA_Flag = 1;
    FLASH_Lock(FLASH_MEMTYPE_PROG);
}

void OTA_ClearUpdateFlag(void)
{
    OTA_Flag = 0;
}

bool OTA_Check(void)
{
	return OTA_Flag == 1;
}

void OTA_Update(void)
{
    uint8_t buff[BLOCK_BYTES];
    
    uint8_t* ota = (uint8_t *)OTA_CODE_START_ADDR;
    
    uint16_t packages = (OTA_Bin_Size >> 7) + ((OTA_Bin_Size & 0x7F) ? 1 : 0);
    
    for(uint16_t i = 0; i < packages; i++)
    {
        for(uint8_t j = 0 ; j < BLOCK_BYTES; j++)
            buff[j] = (*(PointerAttr uint8_t *) (MemoryAddressCast)(ota + i * BLOCK_BYTES + j)); 
        
        
        FLASH_ProgramBlock(APP_BLOCK_NUM_START + i, FLASH_MEMTYPE_PROG, FLASH_PROGRAMMODE_STANDARD, buff);
    }
    
}

//void OTA_Info(uint8_t *data)
//{
//	total_packages = (uint16_t)(*(data+1)) << 8 + *data;
//	fw_checksum = (uint32_t)(*(data + 3)) << 24 + (uint32_t)(*(data + 2)) << 16 + (uint32_t)(*(data + 1)) << 8 + *data;
//}
