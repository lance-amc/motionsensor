/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Scheduler.c
 * @author  lance
 *
 ********************************************************************************************************
 */


#include <string.h>
#include "stm8s.h"
#include "Scheduler.h"
#include "Clock.h"

#include "MemPool.h"

#define MAX_TASKS	8

static Handle _pHead;
static uint8_t _maxTasks;
static bool needService = false;
static bool inited = false;
static uint16_t tick_counter;

__no_init __eeprom uint16_t volatile lifetime;

static  void update_life_time(void)
{
    if(++tick_counter == TICK_FREQUENCE * 60) {
        tick_counter = 0;
        ++lifetime;
    }
}

void Scheduler_Init()
{
	if (!inited) {
		_maxTasks = MAX_TASKS;
		needService = false;
		_pHead = (Handle)MemPool_Get((uint8_t)(_maxTasks * sizeof(Task)));
		Clock_Init();
        Scheduler_AddTask(update_life_time);
		inited = true;
	}
}

void Scheduler_DeInit(void)
{
	_maxTasks = 0;
	needService = false;
	if (_pHead)
		MemPool_Put(_pHead);
}

static Handle findFirstFree(void) {
	uint8_t i ;
	Handle cursor = _pHead;
	for (i = 0; i < _maxTasks; ++i) {
		if (*(cursor + i) == NULL)
			return cursor+i;
	}
	return NULL;
}

Handle Scheduler_AddTask(Task task)
{
	Handle cursor = findFirstFree();

	if (cursor != NULL)
		*cursor = task;

	return cursor;
}


void Scheduler_RemoveTask(Handle handle)
{
	*handle = NULL;
}

void Scheduler_Tick(void)
{
	needService = true;
    tick_counter++;
}

static void RunTasks(void) 
{
	uint8_t i ;
	Handle cursor = _pHead;
	for (i = 0; i < _maxTasks; ++i) {
		if (*(cursor + i) != NULL)
			(*(cursor+i))();
	}
	
}

void Scheduler_Run(void)
{
	if (!needService)
		return;

	needService = false;
	RunTasks();
}
