/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    comm_data.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "comm_data.h"

#include "LightSensor.h"

#define ON		1
#define OFF		0

static uint8_t triggleState;
static uint8_t senserRange;
static uint8_t sensitivity;
static uint8_t sensorState;
static uint16_t lightLevel;

void CommData_SetMotionSensorState(uint8_t data)
{
    sensorState = data;
}

uint8_t CommData_GetMotionSensorState(void)
{
	return sensorState;
}

uint16_t CommData_GetProductLifeTime(void)
{
 extern  __no_init uint16_t __eeprom lifetime;
	return lifetime;
}


void CommData_SetProductLifeTime(uint16_t newLifeTime)
{
    
}

uint8_t CommData_GetSensitivity(void)
{
    return sensitivity;
}

void CommData_SetSensitivity(uint8_t data)
{
    sensitivity = data;
}


uint8_t CommData_GetTriggerState(void)
{
    return triggleState;
}


void CommData_UpdateMsState(uint8_t state)
{
    bool update = false;
    
    if (state == 0x59) {
        update = true;
    }
    
    if (state == 0x4E) {
        update = false;
    }
    
    if (update)
        triggleState = state;
    
}


uint8_t CommData_GetRangeOfDetection(void)
{
    return senserRange;
}


void CommData_SetRangeOfDetection(uint8_t range)
{
    senserRange = range;
}

uint16_t CommData_GetLightLevel(void)
{
    return lightLevel;
}

void CommData_SetLightLevel(uint16_t newLightLevel)
{
    lightLevel = newLightLevel;
}

void CommData_SetLightSensor(uint8_t addr, uint16_t newSetting)
{
    LightSensor_Write(addr, newSetting);
}

uint16_t CommData_GetLightSensor(uint8_t addr)
{
    return LightSensor_Read(addr);
}