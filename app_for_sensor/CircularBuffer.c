/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    CircularBuffer.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "CircularBuffer.h"
#include "MemPool.h"
#include <stdbool.h>

struct CircularBufferStruct{
	uint8_t *buffer;
	uint8_t size;
	uint8_t *head;
	uint8_t *tail;
};
	
uint8_t ITC_GetCPUCC(void)
{
#ifdef _COSMIC_
  _asm("push cc");
  _asm("pop a");
  return; /* Ignore compiler warning, the returned value is in A register */
#elif defined _RAISONANCE_ /* _RAISONANCE_ */
  return _getCC_();
#else /* _IAR_ */
  asm("push cc");
  asm("pop a"); /* Ignore compiler warning, the returned value is in A register */
#endif /* _COSMIC_*/
}

CircularBuffer CircularBuffer_Init(uint8_t *buffer, uint8_t size)
{
	CircularBuffer cb = 
		(CircularBuffer)MemPool_Get(sizeof(struct CircularBufferStruct));
	if (cb) {
		cb->buffer = buffer;
		cb->size = size;
		cb->head = buffer;
		cb->tail = buffer;
	}
	return cb;
}

void CircularBuffer_DeInit(CircularBuffer cb)
{
	if(cb)
		MemPool_Put(cb);		
}

bool CircularBuffer_IsEmpty(CircularBuffer cb)
{
	return (bool)(cb->head == cb->tail);
}

bool CircularBuffer_IsFull(CircularBuffer cb)
{
	return (bool)(((cb->head+1) == cb->tail) || 
		((cb->tail + (cb->size -1)) == cb->head));
}

static void advanceHead(CircularBuffer cb)
{
	if (++cb->head == (cb->buffer + cb->size))
		cb->head = cb->buffer;
}

static void advanceTail(CircularBuffer cb)
{
	if (++cb->tail == (cb->buffer + cb->size))
		cb->tail = cb->buffer;
}

// @return : size of push failed
uint8_t CircularBuffer_Push(CircularBuffer cb, const uint8_t * const src, uint8_t size)
{
	uint8_t *cursor;
	if (!size)
		return 0;
	
//  	bool isISR = FALSE;
	cursor = (uint8_t *)src;
//	uint8_t a = ITC_GetCPUCC();
//  	if ((a & (uint8_t)3) ^ 2)
//	  	isISR = TRUE;
//	if (!isISR)
//  		disableInterrupts();
	while (!CircularBuffer_IsFull(cb) && size--) {
		*(cb->head) = *cursor++;
		advanceHead(cb);
	}
//	if (!isISR)
//		enableInterrupts();
	return (uint8_t)(cursor-src);
}

uint8_t CircularBuffer_Pop(CircularBuffer cb, uint8_t *dest, uint8_t size)
{	
	uint8_t * cursor;
	if (CircularBuffer_IsEmpty(cb) || !size)
		return 0;
	
//  	bool isISR = FALSE;
	cursor = dest;
//	uint8_t a = ITC_GetCPUCC();
//  	if ((a & (uint8_t)3) ^ 2)
//	  	isISR = TRUE;
//	if (!isISR)
//  		disableInterrupts();

	do {
		*cursor++ = *cb->tail;
		advanceTail(cb);
		--size;
	} while(!CircularBuffer_IsEmpty(cb) && size);
//	if (!isISR)
//		enableInterrupts();
	return (uint8_t)(cursor-dest);
}

