/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    IC_OPT3001.h
 * @author  lance
 *
 ********************************************************************************************************
 */


#ifndef IC_OPT3001_H
#define IC_OPT3001_H

#include "stm8s.h"

#define RESULT_ADDR             (0x00)
#define CONFIGURATION_ADDR      (0x01)
#define LOW_LIMIT_ADDR          (0x02)
#define HIGH_LIMIT_ADDR         (0x03)
#define MANUFACTURER_ID         (0x7E)
#define DEVICE_ID               (0x7F)

#define REGISTER_NUMBER         6


void IC_OPT3001_Init(void);

#endif //IC_OPT3001_H