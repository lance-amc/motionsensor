/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Clock.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#ifndef CLOCK_H
#define CLOCK_H

#include "stm8s.h"

#define TICK_FREQUENCE 100

void Clock_Init(void);
void Clock_Suspend(void);
void Clock_Restart(void);

#endif // CLOCK_H