#ifndef LOW_IO_H
#define LOW_IO_H

#include "stm8s.h"
#include "string.h"

void LowIO_Init(void);
void LowIO_Write(uint8_t* data, uint8_t size_data);
uint8_t LowIO_Read(uint8_t* data, uint8_t data_size);

#endif