/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Scheduler.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include "stm8s.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*Task)(void);
typedef Task *Handle;

void Scheduler_Init(void);
void Scheduler_DeInit(void);
Handle Scheduler_AddTask(Task task);
void Scheduler_RemoveTask(Handle handle);
void Scheduler_Tick(void);
void Scheduler_Run(void);

#ifdef __cplusplus
}
#endif

#endif
