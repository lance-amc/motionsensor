#ifndef APP_IO_H
#define APP_IO_H

#include "stm8s.h"
#include "string.h"

//typedef uint8_t size_t; 
//#define ENABLE_485

#ifdef ENABLE_485

#define RE_PORT_485     GPIOB
#define RE_PIN_485      GPIO_PIN_0

#define DE_PORT_485     GPIOB
#define DE_PIN_485      GPIO_PIN_1

#endif

void AppIO_Init(void);
void AppIO_Write(uint8_t* data, uint8_t data_size);
uint8_t AppIO_Read(uint8_t* data, uint8_t data_size);

#endif