/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    IC_OPT3001.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "IC_OPT3001.h"
#include "stm8s_i2c.h"
#include "stm8s_flash.h"
#include "Scheduler.h"
#include "comm_data.h"
#include <stdbool.h>
#include "common.h"
#include "LightSensor.h"

#define I2C_DEVICE_ADDRESS     (0x88)        // 10001000

#define OPT2_ADDR       (0x4803)
#define OPT2_I2C_MASK   ((uint8_t)1 << 6) 
#define MASTER_ADDRESS      (0xAA)          // AA
#define TIMEOUT_INITIAL_VALUE   0x1FFF
#define TIMEOUT_STEP_VALUE  0x100


#define WhileTimeoutWrap(action, condition)  { \
    action; \
    uint16_t timeout = _timeout; \
    while(!condition) { \
        if(!--timeout) { \
            _timeout = ((_timeout > TIMEOUT_STEP_VALUE) ? (_timeout - TIMEOUT_STEP_VALUE) : TIMEOUT_STEP_VALUE); \
            return false; \
        } \
    } \
    _timeout += ((_timeout < TIMEOUT_INITIAL_VALUE) ? (TIMEOUT_STEP_VALUE >> 4) : 0); \
} \
    
//    _timeout = (((0xFFFF - _timeout) >  TIMEOUT_STEP_VALUE) ? (_timeout + TIMEOUT_STEP_VALUE) : (0xFFFF)); \

typedef enum {READ, WRITE} CmdType;

const uint8_t register_list[] = {RESULT_ADDR, CONFIGURATION_ADDR, LOW_LIMIT_ADDR, HIGH_LIMIT_ADDR, MANUFACTURER_ID, DEVICE_ID};

static uint16_t register_copy[REGISTER_NUMBER];


static bool IC_OPT3001_Read(uint16_t *ret);
static bool IC_OPT3001_Write(uint8_t addr, uint16_t newValue);
static bool IC_OPT3001_SetRegister(uint8_t Byte_addr);

static uint16_t _timeout = TIMEOUT_INITIAL_VALUE;

static void IC_OPT3001_Task(void)
{
        IC_OPT3001_SetRegister(RESULT_ADDR);
        IC_OPT3001_Read(&register_copy[0]);
}

static uint8_t get_index_by_addr(uint8_t addr)
{
    uint8_t i;
    
    for(i = 0; i < ARRAY_SIZE(register_list); ++i)
    {
        if(addr == register_list[i])
            break;
    }
    
    return i;
}

void IC_OPT3001_Init(void)
{
    uint8_t opt2 = FLASH_ReadOptionByte(OPT2_ADDR) >> 8;
    if (!(opt2 & OPT2_I2C_MASK))
        FLASH_ProgramOptionByte(OPT2_ADDR, opt2 | OPT2_I2C_MASK);
    
    opt2 = FLASH_ReadOptionByte(OPT2_ADDR);
    
    
    I2C_Cmd(ENABLE);
    I2C_Init(200000, MASTER_ADDRESS, I2C_DUTYCYCLE_2, I2C_ACK_CURR, I2C_ADDMODE_7BIT, 8);
    
    for(uint8_t i = 0; i < ARRAY_SIZE(register_list); ++i) {
        if(IC_OPT3001_SetRegister(register_list[i]))
            IC_OPT3001_Read(&register_copy[i]);
    }
    
    IC_OPT3001_Write(CONFIGURATION_ADDR, register_copy[1] | 0x0600);
    
    Scheduler_AddTask(IC_OPT3001_Task);

}

        
static bool IC_OPT3001_SetRegister(uint8_t Byte_addr) 
{
    WhileTimeoutWrap(I2C_GenerateSTART( ENABLE), I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT))
    
    WhileTimeoutWrap(I2C_Send7bitAddress((u8)I2C_DEVICE_ADDRESS, I2C_DIRECTION_TX), I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
    
    WhileTimeoutWrap(I2C_SendData((u8)(Byte_addr)),I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    
    I2C_GenerateSTOP(ENABLE);
    
    return true;
}

static bool IC_OPT3001_Read(uint16_t *ret)
{
    WhileTimeoutWrap(I2C_GenerateSTART( ENABLE), I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));
    WhileTimeoutWrap(I2C_Send7bitAddress((uint8_t)I2C_DEVICE_ADDRESS, I2C_DIRECTION_RX), I2C_CheckEvent(I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) // 
    WhileTimeoutWrap(;, I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_RECEIVED))
    uint8_t highByte;
    WhileTimeoutWrap(highByte = I2C_ReceiveData(), I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_RECEIVED))
    uint8_t lowByte = I2C_ReceiveData();
    I2C_GenerateSTOP(ENABLE);
    *ret = ((uint16_t)highByte << 8) + lowByte;
    
    return true;
}

static bool IC_OPT3001_Write(uint8_t addr, uint16_t newValue)
{
    WhileTimeoutWrap(I2C_GenerateSTART(ENABLE), I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT))
    WhileTimeoutWrap(I2C_Send7bitAddress((uint8_t)I2C_DEVICE_ADDRESS, I2C_DIRECTION_TX), I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
    WhileTimeoutWrap(I2C_SendData((uint8_t)(addr)),I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    WhileTimeoutWrap(I2C_SendData((uint8_t)((newValue) >> 8)),I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    WhileTimeoutWrap(I2C_SendData((uint8_t)((newValue) & 0xFF)),I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    I2C_GenerateSTOP(ENABLE);
    return true;
}

void LightSensor_Write(uint8_t addr, uint16_t newValue)
{
    IC_OPT3001_Write(addr, newValue);
}

uint16_t LightSensor_Read(uint8_t addr)
{
    return register_copy[get_index_by_addr(addr)];
}





