/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Eeprom_ext.h
 * @author  lance
 *
 ********************************************************************************************************
 */


#ifndef EEPROM_EXT
#define EEPROM_EXT

void EEPROM_Init(void);

#pragma language=extended

int __eeprom_wait_for_last_operation(void);

void __eeprom_program_byte(unsigned char __near * dst, unsigned char v);

void __eeprom_program_long(unsigned char __near * dst, unsigned long v);

#endif //EEPROM_EXT