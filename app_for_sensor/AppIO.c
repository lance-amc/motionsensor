/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    AppIO.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "AppIO.h"

#include "CircularBuffer.h"
#include "MemPool.h"
#include "string.h"
#include "stm8s.h"
#include "MotionSensor.h"

static CircularBuffer sendCircularBuf;
static CircularBuffer receiveCircularBuf;

#define SEND_BUF_SIZE 250
#define RECEIVE_BUF_SIZE 250

static uint8_t sendBuf[SEND_BUF_SIZE];
static uint8_t receiveBuf[RECEIVE_BUF_SIZE];


#define UART_GOIO GPIOD
#define UART_TX_PIN 5
#define UART_RX_PIN 6

#define ENABLE_RX_INTERRUPT() UART2->CR2 |= ((uint8_t)1 << (uint8_t)5)
#define ENABLE_TX_INTERRUPT() UART2->CR2 |= ((uint8_t)1 << (uint8_t)6)

#define DISABLE_RX_INTERRUPT() UART2->CR2 &= ~((uint8_t)1 << (uint8_t)5)
#define DISABLE_TX_INTERRUPT() UART2->CR2 &= ~((uint8_t)1 << (uint8_t)6)

#define ENABLE_485_RX   {\
    GPIO_WriteLow(DE_PORT_485, DE_PIN_485);\
    GPIO_WriteLow(RE_PORT_485, RE_PIN_485);\
}

#define ENABLE_485_TX   {\
    GPIO_WriteHigh(RE_PORT_485, RE_PIN_485);\
    GPIO_WriteHigh(DE_PORT_485, DE_PIN_485);\
}

/*
 * init uart
 */
static void init_uart2(void)
{
#ifdef ENABLE_485
    GPIO_Init(RE_PORT_485, RE_PIN_485, GPIO_MODE_OUT_PP_LOW_FAST);
    GPIO_Init(DE_PORT_485, DE_PIN_485, GPIO_MODE_OUT_PP_LOW_FAST);
    RE_PORT_485->CR1 |= (1 << RE_PIN_485);
    DE_PORT_485->CR1 |= (1 << DE_PIN_485);
#endif
    
	// set TX pin to PP and RX pin to PullUp input
	UART_GOIO->CR1 |= (1 << UART_TX_PIN);
	UART_GOIO->CR1 &= (~(1 << UART_RX_PIN));

	CLK->PCKENR1 |=  CLK_PCKENR1_UART2;      // enable uart clock

	// Todo: failed when baudrate = 115200
	// set baud rate to 115200
//	UART2->BRR2 = 0x05;   // 16000000 / 115200 = 69 = 0x45  16000000/9600=833(0x341)
//	UART2->BRR1 = 0x04;
    
    	UART2->BRR2 = 0x0A;   // 16000000 / 115200 = 69 = 0x8A  16000000/9600=833(0x341)
	UART2->BRR1 = 0x08;

	// 1 bit start + 8 bit data + 1 bit stop + none flow control
	UART2->CR1 &= (~((uint8_t)1 << 4));

	// enable inter
//	ITC->ISPR6 |= (uint8_t)3;

	
	UART2->SR &= ~((uint8_t)(3 << 6));

	// disable Tx and enable Rx interrupt
	UART2->CR2 &= ~((uint8_t)1 << (uint8_t)6);
	UART2->CR2 |= (1 << 5);

//	UART2->SR &= ~((uint8_t)3 << 6);
	// enable TX and RX
	UART2->CR2 |= (3 << 2);
    
#ifdef ENABLE_485
    ENABLE_485_RX;
    delay_loop(100);
#endif
    
}

static void UART_SendChar(uint8_t c)
{
  	UART2->DR = c;   
}

#ifdef _COSMIC_
void UART2_TX_IRQHandler(void)
#else
INTERRUPT_HANDLER(UART2_TX_IRQHandler, 20)
#endif
{
	/* In order to detect unexpected events during development,
	   it is recommended to set a breakpoint on the following instruction.
	*/
	
	uint8_t temp;
	if (UART2->SR & ((uint8_t)1 << 6))
	 	UART2->SR &= ~((uint8_t)1 << 6);
	
	if (CircularBuffer_Pop(sendCircularBuf, &temp, 1))
	  	UART_SendChar(temp);
	else {
	  	DISABLE_TX_INTERRUPT();
#ifdef ENABLE_485
        delay_loop(100);
        ENABLE_485_RX;
#endif
    }
	
	LowIO_ErrOrIntFlag = true;

}

/**
  * @brief UART2 RX interrupt routine.
  * @param  None
  * @retval None
  */
#ifdef _COSMIC_
	void UART2_RX_IRQHandler(void)
#else
 INTERRUPT_HANDLER(UART2_RX_IRQHandler, 21)
#endif
 {
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
	uint8_t sr = UART2->SR;
	uint8_t temp = UART2->DR;
    
    LowIO_ErrOrIntFlag = true;
	
	if (sr & ((uint8_t)1 << 5))
		sr &= ~((uint8_t)1 << 5);
	
	if (sr &  ((uint8_t)1 << 3))
		return;
	
	CircularBuffer_Push(receiveCircularBuf, &temp, 1);
 }

void AppIO_Init(void)
{
	  // init uart
	init_uart2();
	MemPool_Init();
	
	sendCircularBuf = CircularBuffer_Init(sendBuf, sizeof(sendBuf));
	receiveCircularBuf = CircularBuffer_Init(receiveBuf, sizeof(receiveBuf));
  
}


void AppIO_Write(uint8_t* data, uint8_t data_size)
{
	uint8_t temp;
	CircularBuffer_Push(sendCircularBuf, data, data_size);
	
	if (!(UART2->CR2 & ((uint8_t)1 << (uint8_t)6))) {
		if (CircularBuffer_Pop(sendCircularBuf, &temp, 1)) {
#ifdef ENABLE_485
            ENABLE_485_TX;
            delay_loop(100);
#endif
			UART_SendChar(temp);
			ENABLE_TX_INTERRUPT();
		}
	}
}


uint8_t AppIO_Read(uint8_t* data, uint8_t data_size)
{
	return CircularBuffer_Pop(receiveCircularBuf, data, data_size);
}