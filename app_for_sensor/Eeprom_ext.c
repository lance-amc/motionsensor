/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Eeprom_ext.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "Eeprom_ext.h"
#include "stm8s_flash.h"

void EEPROM_Init(void)
{
  //unlock EEPROM
  FLASH_Unlock(FLASH_MEMTYPE_DATA);

}

#pragma language=extended

int __eeprom_wait_for_last_operation(void)
{
  FLASH_Status_TypeDef status = FLASH_WaitForLastOperation(FLASH_MEMTYPE_DATA);
  return !!(status & (  FLASH_STATUS_SUCCESSFUL_OPERATION
                      | FLASH_STATUS_END_HIGH_VOLTAGE));
}

void __eeprom_program_byte(unsigned char __near * dst, unsigned char v)
{
  FLASH_ProgramByte((u32)dst, (u8)v);
}

void __eeprom_program_long(unsigned char __near * dst, unsigned long v)
{
  FLASH_ProgramWord((u32)dst, (u32)v);
}