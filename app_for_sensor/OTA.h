#ifndef OTA_H
#define OTA_H

#include "stm8s.h"

//typedef enum {
//     FLASH_MEMTYPE_PROG      = (uint8_t)0x00,
//     FLASH_MEMTYPE_DATA      = (uint8_t)0x01 
//} FLASH_MemType_TypeDef;

void OTA_Start(uint16_t binLength);
void OTA_Info(uint8_t *data);
bool OTA_Write(uint8_t *data, uint8_t size);
bool OTA_Check(void);
void OTA_Update(void);
void OTA_SetUpdateFlag(void);
void OTA_ClearUpdateFlag(void);
void OTA_Prepare(void);

#endif //OTA_H