/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    LightSensor.h
 * @author  lance
 *
 ********************************************************************************************************
 */


#ifndef LIGHT_SENSOR_H
#define LIGHT_SENSOR_H

#include "stm8s.h"

void LightSensor_Write(uint8_t addr, uint16_t newValue);
uint16_t LightSensor_Read(uint8_t addr);

#endif 