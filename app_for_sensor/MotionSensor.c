/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    MotionSensor.c
 * @author  lance
 *
 ********************************************************************************************************
 */


#include "MotionSensor.h"
#include "MemPool.h"
#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_conf.h"
#include "LowIO.h"
#include "common.h"
#include "Scheduler.h"
#include "comm_data.h"
#include "Clock.h"

//#define DEBUG_MOTION_SENSOR

/*
 * pin3: D2, 	pin4: D0,	pin5: D4 (digital input), pin6: B0(DAC)(stm8s no dac), 	pin7: C6 (digtal output)
 */	

bool LowIO_ErrOrIntFlag = false;

enum {
	IDLE,
	WAIT_TRANSMIT,
	TRANSMITING,
	TRANSMITED
};

enum {
	COMMAND,
	FIRST_ACK,
	SECOND_ACK,
	RECEIVE,
	SEND
};

typedef struct {
	CommandCallback callback;
	Command cmd;
	uint8_t data_out[10];
	uint8_t data_out_length;
	uint8_t data_in[10];
	uint8_t data_in_length;
} MessageStruct, *MessageStructP;

//static MessageStructP pCurMsg = NULL, pNextMsg = NULL;
#define MAX_MSG     5
static MessageStructP pMsgArray[MAX_MSG];
static MessageStructP pCurMsg;
static MessageStructP* ppCurMsg;

static uint8_t mainState, subState;

static void pins_config(void);

static void pins_config(void)
{	
	GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT);
	GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_SLOW);	
    GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW);
}

static void MotionSensor_Task(void);

static Handle handle;
void MotionSensor_Init(void)
{
	MemPool_Init();
	pins_config();
	mainState = IDLE;
	subState = COMMAND;
	LowIO_ErrOrIntFlag = false;
    ppCurMsg = pMsgArray;
	handle = Scheduler_AddTask(MotionSensor_Task);
}

struct RegularCmdsList {
    uint8_t cmd;
    CommData_SetCB cb;
};

const struct RegularCmdsList regular_cmds_list[] = {
    {READ_MOTION_STATUS, (CommData_SetCB)CommData_UpdateMsState},
//    {READ_RANGE_SETTING, (CommData_SetCB)CommData_SetRangeOfDetection},
//    {READ_SENSITIVITY, (CommData_SetCB)CommData_SetSensitivity}
};

static uint8_t cmdListIndex;

static void Regula_Callback(uint8_t* data, uint8_t size, bool LowIO_ErrOrIntFlag)
{
    if (LowIO_ErrOrIntFlag)
        return;
    
    regular_cmds_list[cmdListIndex].cb(*data);
}


static void MotionSensor_Task(void)
{
    static uint8_t transIntervalCounter = TICK_FREQUENCE / 10;
    static bool transFlag = false;
    
    if (!transFlag && --transIntervalCounter) {
        return;
    }
    transFlag = true;
    transIntervalCounter = TICK_FREQUENCE / 10;
    
    
	if (!pCurMsg) // && pNextMsg)
	{
//		pCurMsg = (MessageStructP)((uint16_t)pCurMsg ^ (uint16_t)pNextMsg);
//		pNextMsg = (MessageStructP)((uint16_t)pCurMsg ^ (uint16_t)pNextMsg);
//		pCurMsg = (MessageStructP)((uint16_t)pCurMsg ^ (uint16_t)pNextMsg);
        if(++ppCurMsg > pMsgArray + MAX_MSG - 1)
            ppCurMsg = pMsgArray;
        
        if(*ppCurMsg != NULL) {
            pCurMsg = *ppCurMsg;
            *ppCurMsg = NULL;
            mainState = WAIT_TRANSMIT;
        }
	}
    
	
	switch(mainState) {
	case IDLE: {
		break;
	}
	case WAIT_TRANSMIT: {
		LowIO_ErrOrIntFlag = false;
		mainState = TRANSMITING;
		subState = COMMAND;
	break;
	}
	
	case TRANSMITING: {
		switch(cmdTypeMap[pCurMsg->cmd - COMMAD_START])
		{
		case READ: {
			switch(subState)
			{
			case COMMAND: {
				LowIO_Write((uint8_t *)&(pCurMsg->cmd), 1);
#ifdef DEBUG_MOTION_SENSOR
		extern bool LowIO_NextReceivedChar(uint8_t c);
				LowIO_NextReceivedChar(0x59);
//				LowIO_NextReceivedChar(0x04);
#endif
				subState = RECEIVE;
			break;
			}
            
			case RECEIVE: {
                
				pCurMsg->data_out_length = LowIO_Read(pCurMsg->data_out, sizeof(pCurMsg->data_out));
                if(!pCurMsg->data_out_length)
                    LowIO_ErrOrIntFlag = true;
                mainState = TRANSMITED;
				break;
                
			}
			default:
				LowIO_ErrOrIntFlag = true;
				mainState = TRANSMITED;
				break;
			}
			break;
		}
		
		case WRITE: {
			switch(subState) {
				case COMMAND: {
					LowIO_Write((uint8_t *)&(pCurMsg->cmd), 1);
					subState = RECEIVE;
				break;
				}
				case RECEIVE: {
					pCurMsg->data_out_length += LowIO_Read(pCurMsg->data_out + pCurMsg->data_out_length, sizeof(pCurMsg->data_out));
					assert(pCurMsg->data_out_length == 1);
					subState = SEND;
				break;
				}
				case SEND: {
					LowIO_Write(pCurMsg->data_in, pCurMsg->data_in_length);
					subState = FIRST_ACK;
				break;
				}
				case FIRST_ACK: {
					pCurMsg->data_out_length += LowIO_Read(pCurMsg->data_out + pCurMsg->data_out_length, 1);
					if(ACK != pCurMsg->data_out[pCurMsg->data_out_length-1]) {
						LowIO_ErrOrIntFlag = true;
					}
					mainState = TRANSMITED;
				break;
				}
				default: {
					LowIO_ErrOrIntFlag = true;
					mainState = TRANSMITED;
					break;
				}	
			}
		break;
		}
		
		case CONFIRMED: {
			switch(subState) {
				case COMMAND: {
					LowIO_Write((uint8_t *)&(pCurMsg->cmd), 1);
					subState = FIRST_ACK;
					break;
				}
				case FIRST_ACK: {
					pCurMsg->data_out_length += LowIO_Read(pCurMsg->data_out + pCurMsg->data_out_length, 1);
					if(ACK != pCurMsg->data_out[pCurMsg->data_out_length-1]) {
						LowIO_ErrOrIntFlag = true;
					}
					subState = SEND;
					break;
				}
				case SEND: {
					LowIO_Write(pCurMsg->data_in, pCurMsg->data_in_length);
					subState = SECOND_ACK;
					break;
				}
				case SECOND_ACK: {
//					uint8_t received = ~ACK;
					pCurMsg->data_out_length += LowIO_Read(pCurMsg->data_out + pCurMsg->data_out_length, 1);
					if(ACK != pCurMsg->data_out[pCurMsg->data_out_length-1]) {
						LowIO_ErrOrIntFlag = true;
					}
					mainState = TRANSMITED;
					break;
				}
				default: {
					LowIO_ErrOrIntFlag = true;
					mainState = TRANSMITED;
					break;
				}
			}
			break;
		}
		
		default: {
			LowIO_ErrOrIntFlag = true;
			mainState = TRANSMITED;
			break;
		}
		}
		break;
	}
	
	case TRANSMITED: {
        if (!LowIO_ErrOrIntFlag && pCurMsg->callback != NULL)
            pCurMsg->callback(pCurMsg->data_out, pCurMsg->data_out_length, LowIO_ErrOrIntFlag);
		mainState = IDLE;
		MemPool_Put(pCurMsg);
		pCurMsg = NULL;
        transFlag = false;
		break;
	}
	}
		   
	
	if (!pMsgArray[0])
	{
//        mainState = IDLE;
		if (++cmdListIndex == sizeof(regular_cmds_list)/sizeof(regular_cmds_list[0]))
            cmdListIndex = 0;
        
        
        MotionSensor_Transmit(Regula_Callback, NULL, 0, regular_cmds_list[cmdListIndex].cmd);
	}
    
}

void MotionSensor_Transmit(CommandCallback func, uint8_t* data, uint8_t size, Command cmd)
{
    for(uint8_t i = 0; i < MAX_MSG; ++i) {
        if(!pMsgArray[i]) {
            pMsgArray[i] = (MessageStructP)MemPool_Get(sizeof(MessageStruct));
            assert(pMsgArray[i] != NULL);
            pMsgArray[i]->cmd = cmd;
            
            for(uint8_t j = 0; j < size; ++j)
                pMsgArray[i]->data_in[j] = *(data + j);
            pMsgArray[i]->data_in_length = size;
            pMsgArray[i]->callback = func;
            pMsgArray[i]->data_out_length = 0;
            break;
        }
    }
}


