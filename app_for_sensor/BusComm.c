/**
 ********************************************************************************************************
 * 
 * @brief	
 * @file    BusComm.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "BusComm.h"
#include "common.h"
#include "MotionSensor.h"
#include "OTA.h"
#include "AppIO.h"
#include "Scheduler.h"
#include "common.h"
#include "comm_data.h"
#include "Clock.h"
#include "Config.h"
#include "system.h"

#define DEBUG 

//
//#define STATE_IDLE 			0x01
#define STATE_WAIT_RECEIVE		0x01
#define STATE_WAIT_RECEIVING	0x02
//#define STATE_DATA_RETURN		0x04
//#define STATE_ACK_RETURN		0x08
//#define STATE_HAS_SENT			0x10
#define STATE_DELAY_RESPONSE    0x20

#define FRAME_MAX_SIZE	250

#define SYNC_POS			0
#define SYNC_SIZE			1
#define LENGTH_POS			1
#define LENGTH_SIZE			1
#define TARGET_ADDRESS_POS	2
#define ADDRESS_SIZE		4
#define SOURCE_ADDRESS_POS	(TARGET_ADDRESS_POS + ADDRESS_SIZE)
#define COMMAND_POS			(SOURCE_ADDRESS_POS + ADDRESS_SIZE)
#define COMMAND_SIZE		2
#define PAYLOAD_START_POS	(COMMAND_POS + COMMAND_SIZE)
#define CRC_SIZE			2
#define TAIL_SIZE			1
#define ACK_POS             12

// commands
#define COMMAND_GENERATE_RANDOM			((uint16_t)0x0001)
#define COMMAND_ACCEPT_MASTER_ID		((uint16_t)0x0002)
#define COMMAND_DELETE_MASTER_ID		((uint16_t)0x0003)
#define COMMAND_RETURN_MASTER_ID		((uint16_t)0x0004)
#define COMMAND_RETURN_PRODUCT_INFO		((uint16_t)0x0010)
#define COMMAND_SET_PRODUCT_INFO		((uint16_t)0x0011)
#define COMMAND_RETURN_ABNORMAL_LIST	((uint16_t)0x0014)
#define COMMAND_RETURN_CURRENT_STATE	((uint16_t)0x0015)
#define COMMAND_SET_PARAMETER			((uint16_t)0x0016)
#define COMMAND_GET_LIFETIME			((uint16_t)0x0017)
#define COMMAND_JOIN_GROUP				((uint16_t)0x0030)
#define COMMAND_QUIT_GROUP				((uint16_t)0x0031)
#define COMMAND_RETURN_GROUP			((uint16_t)0x0033)
#define COMMAND_BROADCAST				((uint16_t)0x0050)
#define COMMAND_READY_UPDATE			((uint16_t)0x00A0)
#define COMMAND_INFO_UPDATED			((uint16_t)0x00A1)
#define COMMAND_PACKAGE_UPDATE			((uint16_t)0x00A2)
#define COMMAND_RESTART					((uint16_t)0x00A3)
#define COMMAND_SET_ID					((uint16_t)0x00E0)

#ifdef DEBUG
#define COMMAND_SENSOR_CONF             ((uint16_t)0xAABB)
#define COMMAND_SENSRO_TEST             ((uint16_t)0xBBCC)
#endif

//RESPONDLIST
#define SUCCESS_YES								((uint8_t)0)
#define NO										((uint8_t)1)
#define BUSY									((uint8_t)2)
#define REFUSE									((uint8_t)3)
#define INTERNAL_FLASH_FAILURE					((uint8_t)4)
#define INTERNAL_EEPROM_FAILURE					((uint8_t)5)
#define EXTERNAL__EEPROM_FAILURE				((uint8_t)6)
#define UNSUPPORTTED_COMMAND					((uint8_t)7)
#define UNSUPPORTTED_PAYLOAD					((uint8_t)8)
#define CHECKSUM_FAILURE						((uint8_t)9)
#define COMMAND_FROM_ILLEGAL_DEVICE				((uint8_t)10)
#define OVER_CURRENT							((uint8_t)11)
#define OVER_VOLTAGE							((uint8_t)12)



#define BCD_POS                             0
#define BCD_SIZE                            4
#define FACTORY_CODE_POS                    BCD_SIZE
#define FACTORY_CODE_SIZE                   1
#define MODEL_POS                           (FACTORY_CODE_POS + FACTORY_CODE_SIZE)
#define MODEL_SIZE                          18
#define LED_DRIVER_MODEL_POS                (MODEL_POS + MODEL_SIZE)
#define LED_DRIVER_MODEL_SIZE               18
#define CURRENT_OUTPUT_ID_POS               (LED_DRIVER_MODEL_POS + LED_DRIVER_MODEL_SIZE)
#define CURRENT_OUTPUT_ID_SIZE              2
#define PERCENTAGE_CUSTOMIZE_CURRENT_POS    (CURRENT_OUTPUT_ID_POS + CURRENT_OUTPUT_ID_SIZE)
#define PERCENTAGE_CUSTOMIZE_CURRENT_SIZE   2
#define HARDWARE_VERSION_POS                (PERCENTAGE_CUSTOMIZE_CURRENT_POS + PERCENTAGE_CUSTOMIZE_CURRENT_SIZE)
#define HARDWARE_VERSION_SIZE               2
#define DEVICE_ID_POS                       (HARDWARE_VERSION_POS + HARDWARE_VERSION_SIZE)
#define DEVICE_ID_SIZE                      4


#define PRODUCT_INFO_SIZE                   (BCD_SIZE + FACTORY_CODE_SIZE + MODEL_SIZE + LED_DRIVER_MODEL_SIZE +\
                                            CURRENT_OUTPUT_ID_SIZE + PERCENTAGE_CUSTOMIZE_CURRENT_SIZE + HARDWARE_VERSION_SIZE +\
                                            DEVICE_ID_SIZE)


#define MASTER_ID_SIZE                      4
__no_init __eeprom uint8_t productInfo[PRODUCT_INFO_SIZE];
__no_init __eeprom uint8_t masterID[MASTER_ID_SIZE];
__no_init __eeprom bool productInfoSaveFlag, masterIdSaveFlag;


static uint8_t frame_buf[FRAME_MAX_SIZE];
static uint8_t state = 0;
static uint8_t errFlag = 0;
static uint32_t delayTime;
//static bool isLegalSlave = false;

typedef enum {ACK_RESPONSE, DATA_RESPONSE, DELAY_RESPONSE, NO_RESPONSE} ResponseToBus;

static bool checkTarget(uint8_t *targetChecked)
{
    if (*(targetChecked+8) == 0x11 && (*(targetChecked+9)) == 0x00)
        return true;
    
    
    if (!productInfoSaveFlag)
        return false;
    
    for (uint8_t i = 0; i < DEVICE_ID_SIZE; ++i) {
        if (targetChecked[i] != productInfo[DEVICE_ID_POS + i])
            return false;
    }
    
	return true;
}

static bool checkSource(uint8_t *sourceChecked)
{
	return true;
}

static bool checkCrc(uint8_t *data, uint8_t size, uint16_t crcExpected)
{
	return get_crc(data, size) == crcExpected;
}

static uint8_t package_state_info(uint8_t *data)
{
    uint8_t *d = data;
    if (errFlag)
        *d++ = BUSY;
    else
        *d++ = SUCCESS_YES;
    
    uint16_t ret_value;
    *d++ = CommData_GetTriggerState();
    
    ret_value = CommData_GetLightLevel();
    *d++ = (uint8_t)((ret_value & (uint16_t)0x00FF));
//    *d++ = CommData_Get
    uint16_t lifetime = CommData_GetProductLifeTime();
    *d++ = (uint8_t)((lifetime & (uint16_t)0x00FF));
    *d++ = (uint8_t)((uint16_t)lifetime >> 8);
    *d++ = CommData_GetTriggerState();
    *d++ = CommData_GetSensitivity();
    
    return (uint8_t)(d - data);
}

static uint8_t package_profuct_info(uint8_t *data)
{
    uint8_t i;
    for (i = 0; i < PRODUCT_INFO_SIZE; ++i) {
        data[i] = *(productInfo + i);
    }
    
    data[i++] = FIRMWARE_SUBMAIN_VERSION;
    data[i++] = FIRMWARE_AMIN_VERSION;
    data[i++] = BUS_PROTOCOL_SUBMAIN_VERSION;
    data[i++] = BUS_PROTOCOL_MAIN_VERSION;
       
    return i;
}

static ResponseToBus command_handle(uint8_t *data, uint8_t *size)
{
	uint16_t cmd = *data + (((uint16_t)(*(data+1)))  << 8);
    data += COMMAND_SIZE;
	ResponseToBus responseType;
	switch(cmd) {
	case COMMAND_READY_UPDATE: {
		responseType = ACK_RESPONSE;
        OTA_Prepare();
		break;
	}
	
	case COMMAND_INFO_UPDATED: {
		responseType = ACK_RESPONSE;
        uint16_t bin_length = (*data) + ((uint16_t)(*(data+1)) << 8);
		OTA_Start(bin_length);;
		break;
	}
	
	case COMMAND_PACKAGE_UPDATE: {
		responseType = ACK_RESPONSE;
		OTA_Write(data, *size);
		break;
	}
	
	case COMMAND_RESTART: {
		responseType = ACK_RESPONSE;
		OTA_Check();
		break;
	}
    
    case COMMAND_RETURN_CURRENT_STATE: {
        *size = package_state_info(data+1) + 3;
        responseType = DATA_RESPONSE;
        break;
    }
    
    case COMMAND_GENERATE_RANDOM: {
        if ((*data == 2) || (!masterIdSaveFlag && !(*data)) ||
            (masterIdSaveFlag && *data)) 
        {
            uint32_t max_random = 0;
            for (uint8_t i = 0; i < 4; ++i) {
                max_random += ((uint32_t)(*(data+i+1)) << (8 * i));
            }
            delayTime = (generate_random()) % ((max_random * (*(data+5)))); 
            responseType = DELAY_RESPONSE;
        }

        break;
    }
    
    case COMMAND_SET_PRODUCT_INFO: {
        for (uint8_t i = 0; i < PRODUCT_INFO_SIZE; ++i) {
            productInfo[i] = *(data + i);
        }
        responseType = ACK_RESPONSE;
        productInfoSaveFlag = true;
        break;
    }
    
    case COMMAND_RETURN_PRODUCT_INFO: {
        *size = package_profuct_info(data+1) + 3;
        responseType = DATA_RESPONSE;
        break;
    }   
#ifdef DEBUG
    // data: 0x53(sensitivity: 0 - 255) 0x52(range: 0 - 7) 0x55(dual: 'Y'(0x59) 'N'(0x4E))
    case COMMAND_SENSOR_CONF: {
        MotionSensor_Transmit((CommandCallback)debug_led, data+1, 1, *data);
        MotionSensor_Transmit((CommandCallback)debug_led, data+3, 1, *(data+2));
        MotionSensor_Transmit((CommandCallback)debug_led, data+5, 1, *(data+4));
        responseType = ACK_RESPONSE;
        break;
    }
    
    case COMMAND_SENSRO_TEST: {
        
        break;
    }
#endif
	
	default:
        errFlag = UNSUPPORTTED_COMMAND;
		responseType = ACK_RESPONSE;
		break;
	}
	
    *data = errFlag;
    if (responseType == ACK_RESPONSE)
        *size = 3;
	
	return responseType;
}

void printResult(uint8_t* data, uint8_t size, bool errFlag)
{
	if (errFlag)
		AppIO_Write("error:", 6);
	else
		AppIO_Write("ok:", 3);
	
	AppIO_Write((uint8_t *)data, size);
}

bool BusComm_IsIdle(void)
{
	return (state == STATE_WAIT_RECEIVE) ? true : false;
}

static uint8_t data_command_size = 0;

static bool isBroadcast(uint8_t *addr)
{
    for (uint8_t i = 0; i < ADDRESS_SIZE; i++) {
        if(*(addr+i) != 0xFF)
            return false;
    }
    return true;
}

static void doResponse(uint8_t responseType)
{
    if (responseType == STATE_DELAY_RESPONSE || responseType == NO_RESPONSE)
        return;
    
    frame_buf[LENGTH_POS] = LENGTH_SIZE + ADDRESS_SIZE * 2 + CRC_SIZE + TAIL_SIZE + data_command_size;
    // exchange address
    for (uint8_t i = 0; i < ADDRESS_SIZE; i++)
    {
        frame_buf[TARGET_ADDRESS_POS + i] ^= frame_buf[SOURCE_ADDRESS_POS + i];
        frame_buf[SOURCE_ADDRESS_POS + i] ^= frame_buf[TARGET_ADDRESS_POS + i];
        frame_buf[TARGET_ADDRESS_POS + i] ^= frame_buf[SOURCE_ADDRESS_POS + i];
    }
    
    if(isBroadcast(&frame_buf[SOURCE_ADDRESS_POS])) {
        for (uint8_t i = 0; i < ADDRESS_SIZE; i++) {
            frame_buf[SOURCE_ADDRESS_POS + i] = productInfo[42 + i];
        }
    }
    
    frame_buf[COMMAND_POS + 1] |= 0x10;
    uint16_t crc = get_crc(frame_buf + LENGTH_POS, frame_buf[LENGTH_POS] - CRC_SIZE - TAIL_SIZE);
    frame_buf[1 + frame_buf[LENGTH_POS] - CRC_SIZE - TAIL_SIZE] = (uint8_t)(crc & 0x00FF);
    frame_buf[1 + frame_buf[LENGTH_POS] - CRC_SIZE - TAIL_SIZE + 1] = (uint8_t)(crc >> 8);
    frame_buf[1 + frame_buf[LENGTH_POS] - 1] = 0x55;
    
    
    
    AppIO_Write(frame_buf, 1 + frame_buf[LENGTH_POS]);
}


static void bus_comm_task(void)
{
	switch (state) {
		
	case STATE_WAIT_RECEIVE: {
		
		while(AppIO_Read(frame_buf + SYNC_POS, 1)) {
			if (frame_buf[SYNC_POS] == 0xAA) {
				state = STATE_WAIT_RECEIVING;
				break;
			}
		}
			
		break;
	}
	
	case STATE_WAIT_RECEIVING: {
        uint8_t package_size = AppIO_Read(frame_buf + LENGTH_POS, sizeof(frame_buf));
        
        if (package_size != frame_buf[LENGTH_POS]) {
            state = STATE_WAIT_RECEIVE;
            break;
        }
        
        if(!checkCrc(frame_buf + LENGTH_POS, frame_buf[LENGTH_POS] - (CRC_SIZE + TAIL_SIZE), 
                     ((uint16_t)(frame_buf[LENGTH_POS + frame_buf[LENGTH_POS] - (CRC_SIZE + TAIL_SIZE) +1]) << 8) + frame_buf[LENGTH_POS + frame_buf[LENGTH_POS] - (CRC_SIZE + TAIL_SIZE) ])	) {
            errFlag = CHECKSUM_FAILURE;
        }

        
        if ((checkTarget(frame_buf + TARGET_ADDRESS_POS) || isBroadcast(frame_buf+TARGET_ADDRESS_POS)) &&
                checkSource(frame_buf + SOURCE_ADDRESS_POS)) {
            data_command_size = frame_buf[LENGTH_POS] - (CRC_SIZE + TAIL_SIZE + ADDRESS_SIZE * 2 + 1);
            ResponseToBus responseType = command_handle(frame_buf + COMMAND_POS, &data_command_size);
            
            if (ACK_RESPONSE == responseType || responseType == DATA_RESPONSE)
                doResponse(responseType);
            if (responseType == DELAY_RESPONSE)
                state = STATE_DELAY_RESPONSE;
            else 
                state = STATE_WAIT_RECEIVE;
            break;
        }
        
        
        state = STATE_WAIT_RECEIVE;
        
		break;
	}
        
    case STATE_DELAY_RESPONSE: {
        if (delayTime < (1000 / TICK_FREQUENCE)) {
             DelayMs(delayTime);
             doResponse(ACK_RESPONSE);
             state = STATE_WAIT_RECEIVE;
        } else {
            delayTime -= (1000 / TICK_FREQUENCE);
        }
        break;
    }
		
	}
}

static Handle handle;

void BusComm_Init(void)
{
    state = STATE_WAIT_RECEIVE;
    Scheduler_Init();
    handle = Scheduler_AddTask(bus_comm_task);
    assert(handle != NULL);
}