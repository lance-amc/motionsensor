/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    common.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#ifndef COMMOM_H
#define COMMOM_H

#include "stm8s.h"

#ifdef  USE_FULL_ASSERT
  #define assert(expr) ((expr) ? (void)0 : assert_failed((u8 *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
  void assert_failed(u8* file, u32 line);
#else
  #define assert(expr) ((void)0)
#endif /* USE_FULL_ASSERT */

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

void intToStr(uint32_t data, char *str);
uint16_t get_crc(uint8_t* p_buf, uint16_t len);
void Get_STM8S_UniqueID(uint8_t *uid );
void DelayMs(uint8_t ms);
uint32_t generate_random(void);

#endif