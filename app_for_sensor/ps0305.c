/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    ps0305.h
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "ps0305.h"

const uint8_t cmdTypeMap[COMMAND_NUMBER] = {
	WRITE	   ,
	WRITE     ,
	WRITE     ,
	WRITE     ,
	WRITE     ,
	WRITE     ,
	UNKNOWN   ,
	UNKNOWN   ,
	WRITE     ,
	WRITE     ,
	WRITE     ,
	UNKNOWN   ,
	WRITE     ,
	WRITE     ,
	UNKNOWN   ,
	WRITE     ,
	WRITE     ,
	UNKNOWN   ,
	WRITE     ,
	WRITE     ,
	UNKNOWN   ,
	CONFIRMED ,
	UNKNOWN   ,
	CONFIRMED ,
	UNKNOWN   ,
	UNKNOWN   ,
	UNKNOWN   ,
	UNKNOWN   ,
	UNKNOWN   ,
	UNKNOWN   ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	READ      ,
	UNKNOWN   ,
	READ      ,
	READ      ,
	READ      ,
	UNKNOWN   ,
	READ      ,
	READ      ,
	UNKNOWN   ,
	READ      ,
	READ      ,
	UNKNOWN   ,
	READ      ,
	READ      ,
	UNKNOWN   ,
	UNKNOWN   ,
	READ      
};