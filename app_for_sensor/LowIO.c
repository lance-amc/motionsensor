/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    LowIO.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "LowIO.h"
#include "CircularBuffer.h"
#include "MemPool.h"
#include "string.h"
#include "stm8s.h"
#include "swuart.h"

static CircularBuffer sendCircularBuf;
static CircularBuffer receiveCircularBuf;


#define SEND_BUF_SIZE 50
#define RECEIVE_BUF_SIZE 50

static uint8_t sendBuf[SEND_BUF_SIZE];
static uint8_t receiveBuf[RECEIVE_BUF_SIZE];


void LowIO_Init(void)
{
	MemPool_Init();
	
	sendCircularBuf = CircularBuffer_Init(sendBuf, sizeof(sendBuf));
	receiveCircularBuf = CircularBuffer_Init(receiveBuf, sizeof(receiveBuf));
    
	  // init uart
	uart_init();							// init pins and variables of SW UART
	uart_receive_enable;					// enable receive process
  
}

uint8_t LowIO_NextSendChar(uint8_t *out)
{
	return CircularBuffer_Pop(sendCircularBuf, out, 1);
}

uint8_t LowIO_NextReceivedChar(uint8_t c)
{
	return CircularBuffer_Push(receiveCircularBuf, (const uint8_t *)&c, 1);
}


void LowIO_Write(uint8_t* data, uint8_t data_size)
{
	uint8_t temp;
	CircularBuffer_Push(sendCircularBuf, data, data_size);
	
	if (!test_status(transmit_in_progress)) {
		if (CircularBuffer_Pop(sendCircularBuf, &temp, 1)) {
			uart_send(temp);
		}
	}
}


uint8_t LowIO_Read(uint8_t* data, uint8_t data_size)
{
	return CircularBuffer_Pop(receiveCircularBuf, data, data_size);
}