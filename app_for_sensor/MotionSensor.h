#ifndef MOTION_SENSOR_H
#define MOTION_SENSOR_H

#include "ps0305.h"
#include "stm8s.h"

extern bool LowIO_ErrOrIntFlag;

typedef uint8_t Command;

typedef void (*CommandCallback)(uint8_t* data, uint8_t size, bool errFlag);

void MotionSensor_Init(void);
void MotionSensor_Transmit(CommandCallback func, uint8_t* data, uint8_t size, Command cmd);

#endif