/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    MemPool.c
 * @author  lance
 *
 ********************************************************************************************************
 */


#include <string.h>
#include "common.h"

#include "MemPool.h"

#define HeadType uint16_t
#define HeadSize sizeof(HeadType)

#define FREE (uint8_t)0
#define USED (uint8_t)1
#define HEADER(size, flag) ((HeadType)(size) << 1 | (flag))
#define USE_HEADER(header, size) HEADER(size, USED)
#define IS_HEADER_USED(header) (header->flag == USED)
#define IS_FREE_HEADER(header) !IS_HEADER_USED(header)
#define NEXT_HEADER(pHeader) (pHeader = (Header)((uint8_t *)(pHeader+1) + pHeader->size))
#define MEM_SIZE (0xFF-1) // max: 0x7F-1
#define MEM_GUARD_SIZE 2
#define GUARDER HEADER(0xFFFF, USED)
#define IS_GUARDER(header) (GUARDER == header->all)
#define UNUSED(x) (void)(x)
#define TOTAL_SIZE (MEM_SIZE + MEM_GUARD_SIZE + HeadSize)

typedef union {
	HeadType all;
	struct {
		HeadType flag: 1;
		HeadType size: 15;
	};
}* Header;

static uint8_t memory[TOTAL_SIZE];
static const Header _head = (Header)&memory[0];

/*static uint8_t * tail;*/
static void MemCombinate(Header first_free, Header second_free);

static void MemCombinate(Header first_free, Header second_free)
{
	assert(first_free);
	assert(second_free);
	first_free->all = HEADER(first_free->size+second_free->size+1, FREE);
}


static void calloc_(const Header pHeader, uint8_t size)
{
	Header newHeader;
	assert(pHeader->size >= size);

	if (pHeader->size != size) {
		newHeader= (Header)((uint8_t *)(pHeader) + size + HeadSize);
		newHeader->all = HEADER(pHeader->size-size-HeadSize, FREE);
	}
	pHeader->all = HEADER(size, USED);

	memset(pHeader+1, 0, size);
}

void MemPool_Init(void)
{
	static bool inited = false;
	if (!inited) {
		_head->size = MEM_SIZE;
		_head->flag = FREE;
		*(HeadType*)(&memory[MEM_SIZE + HeadSize-1]) = GUARDER;
		memset(&memory[HeadSize], 0, MEM_SIZE);
		inited = true;
	}
}

static Header searchFree(Header startingPoint)
{
	assert(startingPoint);
	while(IS_HEADER_USED(startingPoint)) {
		if (IS_GUARDER(startingPoint))
			return NULL;
		NEXT_HEADER(startingPoint);
	}
	return startingPoint;

}

static void tryCombinateFreeWithAdjacent(Header firstFree)
{
	Header second;
	assert(firstFree);
	while(1) {
        second = firstFree;
		NEXT_HEADER(second);
		if (IS_HEADER_USED(second))
			return;
		MemCombinate(firstFree, second);
	}
}

void* MemPool_Get(uint8_t size)
{
	Header head;
	assert(size > 0);
	for(head = searchFree(_head); head; head = searchFree(head)) {

		if (head->size >= size) {
			calloc_(head, size);
			return head+1;
		}

		tryCombinateFreeWithAdjacent(head);
		if (head->size < size) {
			NEXT_HEADER(head);
		}
	}

	return NULL;
}

void MemPool_Put(void *p)
{
	assert(p);
	((Header)p-1)->flag = FREE;
}

#ifdef CPPUTEST
#include "MemPoolSpy_impl.h"
#endif
