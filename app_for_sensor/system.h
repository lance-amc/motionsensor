#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdbool.h>

void clear_restart_flag(void);
void set_restart_flag(void);
bool get_restart_flag(void);
void debug_led(void);

#endif //SYSTEM_H