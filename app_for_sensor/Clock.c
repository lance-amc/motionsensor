/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    Clock.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "Clock.h"
#include "Scheduler.h"
#include "stm8s.h"
#include "stm8s_tim1.h"


/**
  * @brief Timer1 Update/Overflow/Trigger/Break Interrupt routine.
  * @param  None
  * @retval None
  */
#ifdef _COSMIC_
void TIM1_UPD_OVF_TRG_BRK_IRQHandler(void)
#else
INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
#endif
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	TIM1_ClearITPendingBit(TIM1_IT_UPDATE);
	Scheduler_Tick();

}

void Clock_Suspend(void)
{
    TIM1_Cmd(DISABLE);
}

void Clock_Restart(void)
{
    TIM1_Cmd(ENABLE);
}


void Clock_Init(void)
{
	TIM1_TimeBaseInit(9, TIM1_COUNTERMODE_UP, HSE_VALUE/10/TICK_FREQUENCE - 1, 0); //10ms (8000000/80000 = 100)
	TIM1_ARRPreloadConfig(ENABLE);        //??????
	TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE);
	TIM1_Cmd(ENABLE);
}