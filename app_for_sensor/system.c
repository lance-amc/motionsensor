/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    system.c
 * @author  lance
 *
 ********************************************************************************************************
 */


#include "system.h"
#include "stm8s_conf.h"


static bool restartFlag = false;

bool get_restart_flag(void)
{
    return restartFlag;
}

void set_restart_flag(void)
{
	restartFlag = true;
}

void clear_restart_flag(void)
{
    restartFlag = false;
}

void debug_led(void)
{
    GPIO_WriteReverse(GPIOE, GPIO_PIN_5);
}