/**
 ********************************************************************************************************
 * 
 * @brief		
 * @file    main.c
 * @author  lance
 *
 ********************************************************************************************************
 */

#include "stm8s.h"
#include "AppIO.h"
#include "stm8s_conf.h"
#include "LowIO.h"
#include "Scheduler.h"
#include "common.h"
#include "MotionSensor.h"
#include "BusComm.h"
#include "stm8s_IWDG.h"
#include "system.h"
#include "Eeprom_ext.h"
#include "IC_OPT3001.h"


#define SWCR_SWEN_BIT   1
#define SWCR_SWBSY_BIT  0


/* Private variables ---------------------------------------------------------*/

static Handle watchdog_task;

/* Private function prototypes -----------------------------------------------*/
/**
  * @brief Programable loop delay
  * @par Parameters:
  * wt: number of loops
  * @retval None
  */
void delay_loop(volatile u16 wt) {
	while(wt--);
}

/*
 * set system clock to 16M
*/
void init_sysClock(void)
{
    CLK->ECKR |= CLK_ECKR_HSEEN;
    while(!(CLK->ECKR & 0x02)) ;
    CLK->SWCR |=  (1 << SWCR_SWEN_BIT);   // set bit SWEN to 1
    CLK->SWR = 0xB4;          // write SWR to select target clock source
    while(CLK->SWCR & (1 << SWCR_SWBSY_BIT)) ;           // wait for SWBSY cleared 
}

void FeedTheIWDog()
{
  IWDG_ReloadCounter();
}

static void IWDG_Init(void);

void IWDG_Init()
{
  IWDG_SetReload(0xff);      
  IWDG_Enable();
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  
  IWDG_SetPrescaler(IWDG_Prescaler_256); 
  watchdog_task = Scheduler_AddTask(FeedTheIWDog);
}




void main( void )
{
	disableInterrupts();
	init_sysClock();
	AppIO_Init();
	LowIO_Init();
	Scheduler_Init();
	MotionSensor_Init();
	BusComm_Init();
    EEPROM_Init();
    IC_OPT3001_Init();
	IWDG_Init();
	enableInterrupts();
 
	while(1)
	{
		Scheduler_Run();
		while(get_restart_flag() && BusComm_IsIdle())
		{
			Scheduler_RemoveTask(watchdog_task);
            FeedTheIWDog();
            clear_restart_flag();
		}
	}
}

/**
  * @brief Reports the name of the source file and the source line number where
  * the assert error has occurred.
  * User can add his own implementation to report the file name and line number.
  * ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line)
  * @retval void None
  * @par Required preconditions:
  * None
  * @par Called functions:
  * None
  */
#ifdef USE_FULL_ASSERT
void assert_failed(u8 *file, u32 line)
{
  /* Put enter your own code to manage an assert error */
	AppIO_Write((uint8_t *)file, strlen((char const *)file));
	AppIO_Write(": ", 2);
	char str[6];
	intToStr(line, str);
	AppIO_Write((uint8_t *)str, strlen(str));
}
#else
void assert_failed(void)
{
}
#endif

