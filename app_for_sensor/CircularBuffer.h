#ifndef __CIRCULAR_BUFFER_H
#define __CIRCULAR_BUFFER_H

#include "stm8s.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CircularBufferStruct *CircularBuffer;

CircularBuffer CircularBuffer_Init(uint8_t *buffer, uint8_t size);
void CircularBuffer_DeInit(CircularBuffer cb);
uint8_t CircularBuffer_Push(CircularBuffer cb, const uint8_t * const src, uint8_t size);
uint8_t CircularBuffer_Pop(CircularBuffer cb, uint8_t *dest, uint8_t size);

#ifdef __cplusplus
}
#endif


#endif
